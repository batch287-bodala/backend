const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 5001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.os39zjz.mongodb.net/s36-activity",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

app.listen(port, () => console.log(`Currently listening to port ${port} `));

app.use("/tasks", taskRoute);