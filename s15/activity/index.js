let first_name = "John";
let last_name = "Smith";
let Age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let work_address = {
    houseNumber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
}

console.log("First Name: " + first_name);
console.log("Last Name: " + last_name);
console.log("Age: " + Age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(work_address);

let name = "Steve Rogers";
console.log("My full name is: " + name);

let currentAge = 40;
console.log("My current age is: " + currentAge);
    
let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

    username: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false
}
console.log("My Full Profile: ")
console.log(profile);

let fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

let lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);
