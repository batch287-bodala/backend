let num = 2;
let getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave NW", "California", 90011];
const [street, state, pincode] = address;
console.log(`I live at ${street}, ${state} ${pincode}`);

const animal = {
	name:"Lolong",
	species:"salwater crcocdile",
	weight:"1075 kgs",
	length:"20 ft",
	width:"3 in"
}
const {name, species, weight, length, width} = animal;
console.log(`${name} was a ${species}. He weighed ${weight} with a measurement of ${length} ${width}.`);

const array = [1, 2, 3, 4, 5]
array.forEach((num)=>{
	console.log(num);
})

let reduceNumber = array.reduce((x,y) => x+y);
console.log(reduceNumber);

class Dog{
	constructor(name,age,breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let dog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(dog);

