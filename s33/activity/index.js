fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => console.log(json.map(data => data['title'])));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET'
})
.then((res)=>res.json()).then((data)=>console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET'
})
.then((res)=>res.json()).then((data)=>{
	console.log(`The item "${data.title}" on the list has a status of ${data.completed}`);
});


fetch("https://jsonplaceholder.typicode.com/todos",{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1,
	})
})
.then((res)=>res.json()).then((data)=>console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		dateCompleted: "pending",
		status: "pending",
		description: "To update the my to do list with a different data structure",
		userId: 1,
	})
})
.then((res)=>res.json()).then((data)=>console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then((res)=>res.json()).then((data)=>console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'DELETE'
});