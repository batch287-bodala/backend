const express = require("express");

const app = express();

const port = 3002;

app.use(express.json())

app.use(express.urlencoded({extended:false}));

app.get("/home", (request, response) => {
	response.send('Welcome to the home page');
});

let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	}
]; 

app.get("/users", (request, response) => {
	response.send(users);
});

app.delete("/delete-user", (req,res) => {
	let found = 0
	for(let i=0; i<users.length; i++){
		if(req.body.username==users[i].username){
			res.send(`User ${req.body.username} has been deleted`);
			found = 1
			break;
		}
	}
	if(found==0){
		res.send("User not found.")
	}
})


app.listen(port, () => console.log(`Server is currently running at port ${port}`));
