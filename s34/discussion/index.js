// Use the require directive to load the express module/package
// It allows us to access to methods and functions that will allow us to easily create a server
const express = require("express");

// Creates an application using express
// In layman's term, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3001;

// Use middleware to allow express to read JSON
app.use(express.json())

// Use middleware to allows express to able to read more data types from a response
app.use(express.urlencoded({extended:false}));

// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hellow from the /hello endpoint!');
});

app.post("/display-name", (req, res) => {
	console.log(req.body);
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`);
});

// Sign-up in our Users.

let users = []; // This is our mockdatabase

app.post("/signup", (request,response) => {

	console.log(request.body);

	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
		console.log(users);
	} else {
		response.send("Please input BOTH username and password.");
	}

});


app.listen(port, () => console.log(`Server is currently running at port ${port}`));


