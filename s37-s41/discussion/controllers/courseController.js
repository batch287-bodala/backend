const Course = require("../models/Course");

module.exports.addCourse = (data) =>{

	console.log(data)

	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		return newCourse.save().then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		})
	}

	let message = Promise.resolve("User must be an Admin to access this.")
	return message.then((value) => {
		return value
	});
};

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.activeCourses = () => {

	return Course.find({ isActive: true }).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};
