const http = require('http');

const port = 4004;

const server = http.createServer((request, response) => {
	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the server! This is currently running at localhost:4004.')
	} else if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the homepage! This is currently running at localhost:4004.')
	} else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("Page not available!")
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);