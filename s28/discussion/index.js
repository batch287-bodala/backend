// CRUD Operations
    // CRUD Operations are the heart of any backend application.
    // Mastering the CRUD Operations is essential for any developer.




// Insert One Document
    //Syntax:
        // db.collectionName.insertOne({object})

db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "1234567890",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
});

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "1234567890",
            email: "theoryofeverything@mail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "1234567890",
            email: "firstmanonthemoon@mail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

db.users.find();
db.users.find({firstName: "Stephen"});

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "0000000000",
        email: "test@mail.com"
    },
    course: [],
    department: "none"
});

db.users.updateOne(
    {firstName: "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "0987654321",
                email: "billgates@mail.com"
            },
            course: ["PHP", "C++", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
);

db.users.updateMany(
    {department: "none"},
    {
        $set: {department: "HR"}
    }
);

db.users.insertOne({
    firstName: "Test"
});

db.users.deleteOne({
    firstName: "Test"
});
